﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Pravo.Parser.Tests
{
    [TestClass]
    public class SourceTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            ISource<string> source = new StringLinesSource()
            {
                Take = 10,
                Skip = 0
            };

            var results = source.ToList();

            Assert.AreEqual(10, results.Count());
            // ....
        }
    }
}
