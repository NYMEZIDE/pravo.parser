﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pravo.Parser.Tests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public async Task CsvParse_Single()
        {
            IParser<string> parser = new CsvParser(mapper, ";");

            var result = await parser.Parse("12345;name");

            Assert.AreEqual(12345, result.Id);
            Assert.AreEqual("name", result.Name);
        }
    }
}
