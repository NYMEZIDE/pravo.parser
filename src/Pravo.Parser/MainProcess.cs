﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pravo.Parser
{
    public class MainProcess<T>
    {
        private readonly ISource<T> _source;
        private readonly IParser<T> _parser;
        private readonly IStore _store;

        public MainProcess(ISource<T> source, IParser<T> parser, IStore store)
        {
            _source = source;
            _parser = parser;
            _store = store;
        }

        public async Task Process()
        {
            foreach(var s in _source)
            {
                await _store.Save(await _parser.Parse(s));
            }
        }
    }
}
