﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pravo.Parser
{
    public interface ISource<T> : IEnumerable<T>
    {
    }
}
