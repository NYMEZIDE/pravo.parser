﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pravo.Parser
{
    public class Result
    {
        public Result(long id, string name)
        {
            Id = id;
            Name = name;
        }

        public long Id { get; private set; }

        public string Name { get; private set; }

        // поля согласно бизнес логике для сохранения
    }
}
